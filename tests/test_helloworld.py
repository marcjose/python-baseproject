#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Test HelloWorld class functions.
"""

import unittest

from src.run import main
from src.helloworld import HelloWorld


class HelloWorldTestCase(unittest.TestCase):
    """
    @HelloWorld tests.
    """

    def test_hello_world(self) -> None:
        """
        Test creating @HelloWorld object and setting its text.
        """
        printer: HelloWorld = HelloWorld(text='Hello World!')
        self.assertTrue(printer.text == 'Hello World!')
        printer.set_text('Hello again!')
        self.assertTrue(printer.text == 'Hello again!')


class EntryPointTest(unittest.TestCase):
    """
    Test entry point.
    """

    def test_entry_point(self) -> None:
        main()
